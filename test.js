const client = new (require('./client'))('9ea0a07cbf2f8885cf396b9d861c85c855a5cfcc07904e220b45857d25d44c59');

async function test() {
  const message = {'this': 'shit', 'is': 'epic'};

  let post = await client.save('test/message', message).catch(console.error);
  console.log(`POST: ${JSON.stringify(post)}\n`);

  let get = await client.fetch('test/message').catch(console.error);
  console.log(`GET: ${JSON.stringify(get)}`);

  let del = await client.delete('test/message').catch(console.error);
  console.log(`DELETE: ${JSON.stringify(del)}`);

  let getAgain = await client.fetch('test/message').catch(console.error);
  console.log(`GET: ${JSON.stringify(getAgain)}`);
}

test();