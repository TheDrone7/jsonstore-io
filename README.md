# async-jsonstore-io

`async-jsonstore-io` is a library to ease the use of the services provided by [jsonstore.io](https://www.jsonstore.io) to save and retrieve data securely in [JSON](https://json.org/) format.

---

## Installation

To install the library, simply use the following command in your preferred terminal.

```powershell
$ npm i async-jsonstore-io --save
```

This will install the library and also add it to your `package.json` file.

---

## Usage

First of all, get your token by following the steps given below: -

* Visit [jsonstore.io](https://www.jsonstore.io) and click on the **COPY** button.
  ![Get link](https://i.imgur.com/h8f2lGP.png)
* Paste the copied link anywhere you feel like.
* The 64-character-long string that can be seen after `https://www.jsonstore.io/` will be referred to as your `token`.
  ![Get token](https://i.imgur.com/6aTx6KV.png)

Now that you have your token, keep it somewhere safe so you don't lose it ***ever*** .

### Importing the library

First of all, import the client using the `require` function.

```javascript
const JsonStoreClient = require('async-jsonstore-io')
```

---

### Initiating the client

Create a new instance of the client.

```javascript
const jsClient = new JsonStoreClient('token_goes_here')
```

And now you're ready to go!

---

### Saving data

The `save()` method is used to store the information.

**FORMAT :** `jsclient.save(key, data);`

**PARAMETERS: -**

* **key ( string ) :** The key to access your data.
* **data ( any ) :** The data that is stored in the key.

**RETURNS :** The inserted object if the operation was successful.

**RAISES :** Promise Rejection if some error occurred of the format specified [here](#error).

**EXAMPLE: -**

```javascript
const JsonStoreClient = require('async-jsonstore-io');
const client = new JsonStoreClient('token-here');

let obj = {
    'this': 'is',
    a : 'sample',
    'object' : 'to send'
}

// Any object that can be parsed into JSON can be sent
client.save('some/key', obj).then(object=>{
    
    // object = obj
    console.log(object); // will print the object same as obj as 'obj' was inserted
    
    // send can also be used to modify data
    client.send('some/key', 'Some String').then(newObject=>{
        
        // you will now find 'Some String' 
        // when you try to retrieve something from 'some/key'
        
    }).catch(console.error); // Handle promise rejection
    
}).catch(console.error)
```



The `save()` function can also be used to reassign data stored at a key.

---

### Fetching data

The `fetch()` method can be used to retrieve data.

**FORMAT :** `jsclient.fetch(key);`

**PARAMETERS: -**

* **key ( string ) :** The key from which to retrieve your data.

**RETURNS :** The object retrieved if the operation was successful.

**RAISES :** Promise Rejection if some error occurred or there was no object at the specified key as specified [here](#error).

**EXAMPLE : -**

```javascript
const JsonStoreClient = require('async-jsonstore-io');
const client = new JsonStoreClient('token-here');

client.fetch('some/key').then(object=>{
    
    // log the retrieved object
    // is of `any` type
    
    console.log(object);
    
}).catch(console.error); // Handle promise rejection
```



---

### Deleting data

The `delete()` method can be used for this purpose.

**FORMAT :** `jsclient.delete(key);`

**PARAMETERS: -**

* **key ( string ) :** The key where the object to delete was stored.

**RETURNS :** The request response (`{"ok": true}`) if the operation was successful.

**RAISES :** Promise Rejection if some error occurred as specified [here](#error).

**EXAMPLE : -**

```javascript
const JsonStoreClient = require('async-jsonstore-io');
const client = new JsonStoreClient('token-here');

client.delete('some/key').then(del=>{
    
    if(del['ok']){
        // deletion was succesful
    }
    
}).catch(console.error); // Handling promise rejection
```

---

**Note :** *All of the functions listed above are [asynchronous](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function) you should either* `await` *them or use* `.then()` *to handle promises.*

*The use of* `try...catch` *or* `.catch()` *is also recommended to handle promise rejections.*

---

## Error

```json
{
  "code": 404,
  "message": "Not found"
}
```

**code :** The error code returned from the request.

**Message :** A simple error message.

**EXAMPLE : -**

### Listed below are some of the most common error codes: -
1. **ERROR CODE `404`**

    **Name :** Not Found Error.

    This is caused when nothing is found at the specified key while using the `.get()` method.

2. **ERROR CODE `500`**

    **Name :** Internal Server Error. 

    This is caused when an invalid token is given.

---

A working example of the library can be found [here](https://repl.it/@TheDrone7/WeakPromotedColdfusion).