const fetch = require('isomorphic-unfetch');
class JsonStoreClient {
  constructor(token) {
    if (typeof token !== 'string')
      throw new Error('Invalid token passed');
    else if (token.length !== 64)
      throw new Error('Invalid token passed');

    this.token = token;
  }

  /**
   * Save data to the jsonstore database.
   * @param {String} key
   * @param {Object} data
   * @returns {Promise<Object>}
   */
  async save (key, data) {
    return new Promise((resolve, reject) => {
      fetch(`https://www.jsonstore.io/${this['token']}/${key}`, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(data)
      }).then(r => {
        if (r.status === 200 || r.status === 201) return r.json();
        else reject({code: r.status, message: r.statusText});
      }).then(() => { resolve(data) });
    });
  }

  /**
   * Fetch data from jsonstore database.
   * @param {String} key
   * @returns {Promise<Object>}
   */
  async fetch (key) {
    return new Promise((resolve, reject) => {
      fetch(`https://www.jsonstore.io/${this['token']}/${key}`)
        .then(r => {
          if (r.status === 200) return r.json();
          else reject({code: r.status, message: r.statusText});
        }).then(r => {
        if (r['result']) resolve(r['result']);
        else reject({code: 404, message: 'Not found'});
      });
    });
  }

  /**
   * Delete data from the jsonstore database.
   * @param {String} key
   * @returns {Promise<Object>}
   */
  async delete (key) {
    return new Promise((resolve, reject) => {
      fetch(`https://www.jsonstore.io/${this['token']}/${key}`, {method: 'DELETE'})
        .then(r => {
          if (r.status === 200 || r.status === 201) return r.json();
          else reject({code: r.status, message: r.statusText});
        }).then(resolve);
    });
  }
}

module.exports = JsonStoreClient;
